<?php

/**
 * @file
 * Admin page callbacks for the multisite module.
 */

/**
 * Form for configuring multisite settings.
 */
function multisite_settings_form($form, &$form_state) {
  $form = array();

  $form['multisite_base_database'] = array(
    '#type' => 'textfield',
    '#title' => t('Database'),
    '#default_value' => variable_get('multisite_base_database'),
    '#size' => 20,
    '#maxlength' => 50,
    '#description' => t("Database name which should be copied for new subdomain"),
    '#required' => TRUE,
  );

  $form['multisite_base_domain'] = array(
    '#type' => 'textfield',
    '#title' => t('Domain'),
    '#default_value' => variable_get('multisite_base_domain'),
    '#size' => 20,
    '#maxlength' => 100,
    '#description' => t("Domain name for which new subdomains will be created. For Example: example.com"),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Implements hook_form_validate().
 */
function multisite_settings_form_validate($form, &$form_state) {
  $base_database = $form_state['values']['multisite_base_database'];

  if (preg_match('/[^A-Za-z0-9_\-]/', $base_database)) {
    form_set_error('multisite_base_database', t('Only alphanumeric strings and - _ characters are allowed!'));
  }
  if (multisite_database_exists($base_database) == FALSE) {
    form_set_error('multisite_base_database', t('Database %db_name does not exists!', array('%db_name' => $base_database)));
  }
}

/**
 * Implements hook_form().
 */
function multisite_form($form, &$form_state) {
  global $base_url;

  $form = array();

  $errormsg = '';
  if (variable_get('multisite_base_database') == '' || variable_get('multisite_base_domain') == '') {
    $errormsg = '<div class="messages error"><b>Please first set values in ' . l(t('Settings'), 'admin/config/multisite/settings') . ' those are required for sub-domain creation!</b></div><br/><br/>';
    $form['some_text'] = array(
      '#markup' => $errormsg,
    );
  }
  else {
    $form['new_site_name'] = array(
      '#title' => t('Site Name'),
      '#type' => 'textfield',
      '#size' => 20,
      '#maxlength' => 50,
      '#description' => t("Enter new site name"),
      '#required' => TRUE,
    );
    $form['new_domain_name'] = array(
      '#title' => t('Sub Domain Name'),
      '#type' => 'textfield',
      '#size' => 20,
      '#maxlength' => 100,
      '#description' => t("Enter new subdomain. Example test.example.com"),
      '#required' => TRUE,
    );
    $form['new_database_name'] = array(
      '#title' => t('Database Name'),
      '#type' => 'textfield',
      '#size' => 20,
      '#maxlength' => 50,
      '#description' => t("Database name for subdomain"),
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#value' => 'Submit',
      '#type' => 'submit',
    );
    $form['reset'] = array(
      '#type' => 'button',
      '#value' => t('Reset'),
      '#attributes' => array('onclick' => 'this.form.reset(); return false;'),
    );
  }

  return $form;
}

/**
 * Implements hook_form_validate().
 */
function multisite_form_validate($form, &$form_state) {
  $site_name = $form_state['values']['new_site_name'];
  $domain_name = $form_state['values']['new_domain_name'];
  $database_name = $form_state['values']['new_database_name'];

  $domain_folder = DRUPAL_ROOT . '/sites/' . $domain_name;

  // Check if domain folder already exists.
  if (is_dir($domain_folder)) {
    form_set_error('new_domain_name', t('Domain %domain_name already exists!', array('%domain_name' => $domain_name)));
  }
  $error_list = multisite_validate_domain($domain_name);
  foreach ($error_list as $error) {
    form_set_error('new_domain_name', $error);
  }
  if (preg_match('/[^A-Za-z0-9_\-]/', $database_name)) {
    form_set_error('new_database_name', t('Only alphanumeric strings and - _ characters are allowed!'));
  }
  if (multisite_database_exists($database_name) == TRUE) {
    form_set_error('new_database_name', t('Database %db_name already exists!', array('%db_name' => $database_name)));
  }
}

/**
 * Implements hook_form_submit().
 */
function multisite_form_submit($form, &$form_state) {
  $site_name = $form_state['values']['new_site_name'];
  $domain_name = $form_state['values']['new_domain_name'];
  $database_name = $form_state['values']['new_database_name'];

  if (multisite_create_subdomain_folder($domain_name)) {
    $original_site_name = variable_get('site_name');
    variable_set('site_name', $site_name);

    // Copy database...
    multisite_create_domain_database($database_name);

    variable_set('site_name', $original_site_name);

    watchdog('multisite', 'Created domain %site_name (%domain_name)', array('%site_name' => $site_name, '%domain_name' => $domain_name));
    drupal_set_message(t("New domain %domain_name successfully created! <b>Please update database name in sub domain's settings.php file</b>.", array('%domain_name' => $domain_name)));
  }
  $form_state['redirect'] = 'admin/config/multisite';
}

/**
 * Copy database from default site to new domain site.
 */
function multisite_create_domain_database($db_name) {
  $default_db_name = variable_get('multisite_base_database');

  if ($default_db_name != '') {
    // Get default connection.
    $db_default = Database::getConnection('default');

    // Set up a new connection with different connection info.
    $connection_info = Database::getConnectionInfo('default');
    Database::addConnectionInfo('newcon', 'default', $connection_info['default']);
    $db_new = Database::getConnection('newcon');

    // Create new database schema
    // $db_new->query('CREATE DATABASE IF NOT EXISTS '.$db_name);
    $db_new->query('CREATE DATABASE ' . $db_name);

    // Copy tables fron default database to new database.
    $result_tables = $db_default->query('SHOW TABLES FROM ' . $default_db_name);
    foreach ($result_tables as $row) {
      $col = 'Tables_in_' . $default_db_name;
      $table = $row->$col;
      $db_new->query('DROP TABLE IF EXISTS ' . $db_name . '.' . $table . '');
      $db_new->query('CREATE TABLE ' . $db_name . '.' . $table . ' LIKE ' . $default_db_name . '.' . $table . '');
      $db_new->query('INSERT INTO ' . $db_name . '.' . $table . ' SELECT * FROM ' . $default_db_name . '.' . $table . '');
    }
    // Close the connection.
    Database::closeConnection('newcon');
    return TRUE;
  }
  return FALSE;
}

/**
 * Function to check if database exists.
 */
function multisite_database_exists($database) {
  // Set up a new connection with different connection info.
  $connection_info = Database::getConnectionInfo('default');
  Database::addConnectionInfo('dbcon', 'default', $connection_info['default']);
  $db = Database::getConnection('dbcon');

  // Copy tables fron default database to new database.
  $result_tables = $db->query('SHOW DATABASES');
  $db_found = FALSE;
  foreach ($result_tables as $row) {
    if ($row->Database == $database) {
      $db_found = TRUE;
      break;
    }
  }
  // Close the connection.
  Database::closeConnection('dbcon');
  return $db_found;
}
