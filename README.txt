---------------------------------------------------------------------
Multisite Module for Drupal 7.x by Swati Jha
---------------------------------------------------------------------

DESCRIPTION:
----------------

This module simplifies the process of converting single site to multisite.

Multisites is a capability of Drupal to run many web sites out of one
installation of Drupal. With multisite setup, you can have different domains
and subdomains and paths mapped to different directories within /sites.
So, for instance, while Drupal multisites can support two different websites
like example.com and foo.example.com,vit can also support different settings
and databases for example.com and foo.example.com.

FEATURES:
----------------

For Drupal 7 multisite setup following steps are required:

1. Install Drupal 7 the normal way on the first domain (example.com).

2. Make your DNS work for the second site's domain (foo.example.com) to point
to same IP.

3. Make your Web server work for the second site's domain to point to same
directory. (for apache this can be adding the second domain as an alias in the
current host or making a new virtual host that has the same document root)

4. Make a new directory with name equal to the second site's domain in under
the sites directory (drupal7/sites/foo.example.com)

5. Copy default.settings.php to the second site's directory as settings.php
(drupal7/sites/foo.example.com/settings.php) and then chmod it to 666

6. Make a new directory called files (drupal7/sites/foo.example.com/files)
and chmod this new directory to 777.

7. Either :
a. Navigate to the second domain (foo.example.com) and install Drupal normally.
OR
b. Create another database for second site and import tables from first domain
database (created for example.com). Update database name in second site's
settings.php.

8. Navigate to the second domain (foo.example.com).

Multisite module automates the steps from 4 to 7. On a single click you can
create directory structure and database required to setup second site.

INSTALLATION:
----------------

* Put the Multisite module in your sites/all/modules directory and enable

* Go to admin/config/multisite/settings and provide your base database
(second sites' database will be a copy of this database) and base domain name
(for ex: example.com).

* Go to admin/config/multisite/create and enter the details and submit it.
This process:
 - Will create <subdomain> folder under sites/ directory
 - Will copy settings.php from default directory to your <subdomain> directory
 - Will copy files folder from default directory to your <subdomain> directory
 - Will create db for <subdomain> and import tables from given base database

NOTE: After completing above steps you need to follow steps from 1 to 3
as listed in Features' section.
